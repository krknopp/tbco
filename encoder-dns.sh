#!/bin/bash

for encoder in $(cat encoder-list-tbco.txt); do
  { echo "root"; sleep 1; echo "sed -i s/0.0.0.0/8.8.8.8/ /etc/resolv.conf\r"; sleep 1; echo "sed -i '2,/8.8.8.8/ s/8.8.8.8/8.8.4.4/' /etc/resolv.conf\r"; sleep 1; } | telnet $encoder
done
